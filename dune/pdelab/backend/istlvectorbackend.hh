// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_PDELAB_BACKEND_ISTLVECTORBACKEND_HH
#warning "The file dune/pdelab/backend/istlvectorbackend.hh is deprecated. Please use dune/pdelab/backend/istl/istlvectorbackend.hh instead."
#include <dune/pdelab/backend/istl/istlvectorbackend.hh>
#endif // DUNE_PDELAB_BACKEND_ISTLVECTORBACKEND_HH
